# Setting Up Neovim as A System Verilog IDE
This Guide will assume a fresh neovim install, however if you already use 
and configure neovim, the setup detailed here is minimal enough, that you should
be able to adapt it into your own config with no issue.

## Requirements
1. Neovim =>0.7,
2. A Neovim Plugin manager (I recommend (packer.nvim)[])
3. Verilator

## Current Features

### Linting
Fully featured, live, in-line linting and diagnostics support.

![Linting Showcase](https://codeberg.org/Guilty/VerilogImproved/raw/branch/master/linting_showcase.png)

### Advanced Navigation
Navigate to next and previous definiton of identifier (e.g. signal or variable name),
Navigate to the definition of current identifier and list all definitions in the current file
all via neovim's treesitter integrations.

**TODO Insert GIF showcasing functionality**

## Step by Step
### Installing Verilator
Verilator provides clear and easy install instructions for a variety of install methods on
it's website [here](https://verilator.org/guide/latest/install.html).

### Installing Neovim
Similarly install instructions for Neovim are available on their [github repo](https://github.com/neovim/neovim/wiki/Installing-Neovim).

### Installing a plugin manager
If you are already a Neovim user and have a preffered plugin manager, feel free to
install the required plugins (**TODO have req plugins** link to part of readme where plugins are listed)
using it. However, my recommendation would be that you use the wonderful [packer.nvim](https://github.com/wbthomason/packer.nvim),
which you can install with this simple command:  

```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

### Installing the Required Plugins
Now that You have installed Verilator, Neovim & a plugin manager, it is time to install the requisite plugins
here is a list of all the required plugins (which are all hosted on [github.com](https://github.com)):  
  - [wbthomason/packer.nvim](https://github.com/wbthomason/packer.nvim)
  - [hrsh7th/nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
  - [hrsh7th/cmp-buffer](https://github.com/hrsh7th/cmp-buffer)
  - [hrsh7th/cmp-path](https://github.com/hrsh7th/cmp-path)
  - [hrsh7th/cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline)
  - [saadparwaiz1/cmp_luasnip](https://github.com/saadparwaiz1/cmp_luasnip)
  - [hrsh7th/cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp)
  - [hrsh7th/cmp-nvim-lua](https://github.com/hrsh7th/cmp-nvim-lua)
  - [neovim/nvim-lspconfig](https://github.com/neovim/nvim-lspconfig)
  - [nvim-treesitter/nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
  - [jose-elias-alvarez/null-ls.nvim](https://github.com/jose-elias-alvarez/null-ls.nvim)  
  
If you are using `packer.nvim` then you can simply use the `plugins.lua` file provided in this repository.

### Configuring Neovim
4. Put Configuration dir in this repo into,
5. Add the following to your vimrc (require 'nameofthefolder')

### Trouble Shooting


## Roadmap
- [x] Linting    
- [x] Treesitter 
- [ ] Formatting 
- [ ] Snippets   

# Was this Helpful?
If it was, plz gib star :crab:.  
If you have any issues feel free to open an issue on the repo (after checking the Troubleshooting section first)    
Have some tips/ improvements? submit a pull request.
